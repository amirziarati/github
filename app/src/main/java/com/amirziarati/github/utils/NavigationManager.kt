package com.amirziarati.github.utils

interface NavigationManager {
    fun showMailToView(userMail : String)
    fun showWebPage(url : String)
}