package com.amirziarati.github.utils

public interface ConnectionManager {
    fun isConnected(): Boolean
}