package com.amirziarati.github.utils

import android.app.Application
import io.realm.Realm


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
//        DaggerMyApplicationComponent
//                .builder()
//                .myApplicationModule(MyApplicationModule(this))
//                .build()
//                .inject(this)
    }
}