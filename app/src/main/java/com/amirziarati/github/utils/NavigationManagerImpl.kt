package com.amirziarati.github.utils

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri


class NavigationManagerImpl(private val context: Context) : NavigationManager {

    override fun showMailToView(userMail: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse("mailto:$userMail")
        emailIntent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(emailIntent)
    }

    override fun showWebPage(url: String) {
        val webIntent = Intent(Intent.ACTION_VIEW)
        webIntent.data = Uri.parse(url)
        webIntent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(webIntent)
    }
}