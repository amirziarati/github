package com.amirziarati.github.utils

interface Configs{
    companion object {
        val BASE_URL = "https://api.github.com/"
        val LOGGER_TAG = "GithubReposLogger"
        val DEFAULT_USER_NAME = "JakeWharton"
        val LIST_TRESHHOLD_COUNT = 3
        val PAGE_SIZE = 15
    }
}