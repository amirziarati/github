package com.amirziarati.github.utils

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import javax.inject.Inject


class ConnectionManagerImpl(private val context: Context) : ConnectionManager {

    override fun isConnected() : Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return (connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected)
    }
}