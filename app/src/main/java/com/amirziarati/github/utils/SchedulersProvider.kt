package com.amirziarati.github.utils

import io.reactivex.Scheduler

interface SchedulersProvider {
    fun ioThread(): Scheduler
    fun workerThread(): Scheduler
    fun mainThread(): Scheduler
}