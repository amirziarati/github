package com.amirziarati.github.di

import com.amirziarati.github.repo.interactor.GithubInteractor
import com.amirziarati.github.ui.repolist.RepoPresenter
import com.amirziarati.github.ui.repolist.RepoPresenterImpl
import com.amirziarati.github.ui.repolist.RepoView
import com.amirziarati.github.utils.NavigationManager
import dagger.Module
import dagger.Provides


@Module
class RepoModule(private val repoView: RepoView) {

    @Provides
    fun provideRepoPresenter(githubInteractor: GithubInteractor,
                             navigationManager: NavigationManager): RepoPresenter
            = RepoPresenterImpl(repoView, githubInteractor, navigationManager)
}