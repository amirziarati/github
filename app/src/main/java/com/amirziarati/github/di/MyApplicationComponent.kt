package com.amirziarati.github.di

import com.amirziarati.github.ui.repolist.ReposListActivity

import dagger.Component


@Component(modules = [RepoModule::class, MyApplicationModule::class])
interface MyApplicationComponent {
    fun inject(reposListActivity: ReposListActivity)
}