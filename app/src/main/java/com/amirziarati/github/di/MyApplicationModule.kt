package com.amirziarati.github.di

import android.content.Context
import com.amirziarati.github.R
import com.amirziarati.github.repo.interactor.GithubInteractor
import com.amirziarati.github.repo.interactor.GithubInteractorImpl
import com.amirziarati.github.repo.interactor.RemoteResponseMapper
import com.amirziarati.github.repo.interactor.RemoteResponseMapperImpl
import com.amirziarati.github.repo.local.GithubLocalService
import com.amirziarati.github.repo.local.GithubLocalServiceImpl
import com.amirziarati.github.repo.remote.GithubRemoteServiceMock
import com.amirziarati.github.utils.*
import com.amirziarati.githuhrepos.repo.remote.GithubRemoteService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import javax.inject.Named


@Module
class MyApplicationModule(private val context: Context) {

    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String = Configs.BASE_URL

    @Provides
    @Named("mockJson")
    fun provideMockJson(): String {
        return readTextFile(context.resources.openRawResource(R.raw.mock_data))
    }

    fun readTextFile(inputStream: InputStream): String {
        val outputStream = ByteArrayOutputStream()

        val buf = ByteArray(1024)
        var len: Int
        try {
            len = inputStream.read(buf)
            while (len != -1) {
                outputStream.write(buf, 0, len)
                len = inputStream.read(buf)
            }
            outputStream.close()
            inputStream.close()
        } catch (e: IOException) {

        }

        return outputStream.toString()
    }

    @Provides
    fun provideRemoteService(@Named("baseUrl") baseUrl: String): GithubRemoteService {

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()

        return retrofit.create(GithubRemoteService::class.java)
    }

//    @Provides
//    fun provideRemoteService(githubRemoteServiceMock: GithubRemoteServiceMock): GithubRemoteService {
//        return githubRemoteServiceMock
//    }

    @Provides
    fun provideLocalService(githubLocalServiceImpl: GithubLocalServiceImpl): GithubLocalService {
        return githubLocalServiceImpl
    }

    @Provides
    fun provideInteractor(githubInteractorImpl: GithubInteractorImpl): GithubInteractor {
        return githubInteractorImpl
    }

    @Provides
    fun provideSchedulerProvider(schedulersProviderImpl: SchedulersProviderImpl): SchedulersProvider {
        return schedulersProviderImpl
    }

    @Provides
    fun provideConnectionManager(): ConnectionManager {
        return ConnectionManagerImpl(context)
    }

    @Provides
    fun provideNavigationManager(): NavigationManager {
        return NavigationManagerImpl(context)
    }

    @Provides
    fun provideRemoteResponseMapper(remoteResponseMapperImpl: RemoteResponseMapperImpl): RemoteResponseMapper {
        return remoteResponseMapperImpl
    }
}