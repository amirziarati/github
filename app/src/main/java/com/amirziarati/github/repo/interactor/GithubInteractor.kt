package com.amirziarati.github.repo.interactor

interface GithubInteractor {
    fun getRepos(userName: String, page: Int, pageSize: Int, repoCallback: GithubInteractorRepoCallback)
    fun getUserInfo(userName: String, userInfoCallback: GithubInteractorUserInfoCallback)
}