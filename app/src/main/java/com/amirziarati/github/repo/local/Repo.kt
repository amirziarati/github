package com.amirziarati.github.repo.local

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Repo(@PrimaryKey
                var id: Int? = 0,
                var name: String? = "",
                var description: String? = "",
                var starsCount: Int? = 0,
                var updatedAt: Date? = null,
                var license: String? = "") : RealmObject()