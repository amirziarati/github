package com.amirziarati.githuhrepos.repo.remote

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


public interface GithubRemoteService {

    @GET("users/{userName}/repos")
    fun getReposList(@Path("userName") userName: String,
                     @Query("page") page: Int,
                     @Query("per_page") pageSize: Int): Observable<List<GithubReposResponse>>

    @GET("users/{userName}")
    fun getUserInfo(@Path("userName") userName: String): Observable<GithubUserResponse>
}