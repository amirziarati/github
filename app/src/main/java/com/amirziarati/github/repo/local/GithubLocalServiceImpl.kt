package com.amirziarati.github.repo.local

import io.reactivex.Observable
import io.realm.Realm
import javax.inject.Inject

class GithubLocalServiceImpl @Inject constructor() : GithubLocalService {

    private fun getRealm(): Realm {
        return Realm.getDefaultInstance()
    }

    override fun saveRepo(repo: Repo) {
        try {
            getRealm().beginTransaction()
            getRealm().copyToRealmOrUpdate(repo)
            getRealm().commitTransaction()
        } catch (t: Throwable) {

        } finally {
            getRealm().close()
        }
    }

    override fun saveRepos(repos: List<Repo>) {
        try {
            getRealm().beginTransaction()
            getRealm().copyToRealmOrUpdate(repos)
            getRealm().commitTransaction()
        } catch (t: Throwable) {
            //log the problem
        } finally {
            getRealm().close()
        }


    }

    override fun getRepos(userName: String): List<Repo> {
        return getRealm().where(Repo::class.java).findAll().toList()
    }

    override fun getReposList(userName: String, page: Int, pageSize: Int): Observable<List<Repo>> {
        val firstOfPageIndex = (page - 1) * pageSize

        val realmResults = getRealm().where(Repo::class.java).findAllAsync()

        if (firstOfPageIndex > realmResults.size - 1) {
            return Observable.just(ArrayList<Repo>())
        }

        val lastIndex = Math.min(firstOfPageIndex + pageSize, realmResults.size)

        return Observable.just(realmResults.subList(firstOfPageIndex, lastIndex).toList())
    }

    override fun saveUser(user: User) {
        try {
            getRealm().beginTransaction()
            getRealm().copyToRealmOrUpdate(user)
            getRealm().commitTransaction()
        } catch (t: Throwable) {

        } finally {
            getRealm().close()
        }
    }

    override fun getUser(username: String): Observable<User> {
        return Observable.just(getRealm().where(User::class.java).equalTo("userName", username).findFirstAsync())
    }
}