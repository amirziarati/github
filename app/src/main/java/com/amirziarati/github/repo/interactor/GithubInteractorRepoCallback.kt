package com.amirziarati.github.repo.interactor

import com.amirziarati.github.repo.local.Repo

interface GithubInteractorRepoCallback {
    fun onReposReceived(repos: List<Repo>)
    fun onInternetUnavailable()
    fun onServerUnreachable()
    fun onExeededApiCallLimit()
    fun onInternalServerError()
}
