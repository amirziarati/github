package com.amirziarati.github.repo.local

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class User(@PrimaryKey
           var id: Int? = 0,
                var userName: String? = "",
                var displayName: String? = "",
                var avatar: String? = "",
                var mail: String? = "",
                var blog: String? = "",
                var bio: String? = "") : RealmObject()