package com.amirziarati.github.repo.remote

import com.amirziarati.githuhrepos.repo.remote.GithubRemoteService
import com.amirziarati.githuhrepos.repo.remote.GithubReposResponse
import com.amirziarati.githuhrepos.repo.remote.GithubUserResponse
import com.google.gson.Gson
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named
import com.google.gson.reflect.TypeToken
import java.util.concurrent.TimeUnit


class GithubRemoteServiceMock : GithubRemoteService {

    private var repo: List<GithubReposResponse>

    @Inject constructor(@Named("mockJson") mockJson: String) {
        repo = Gson().fromJson(mockJson, object : TypeToken<List<GithubReposResponse>>() {}.type)
    }

    override fun getReposList(userName: String, page: Int, pageSize: Int): Observable<List<GithubReposResponse>> {
        val firstOfPageIndex = (page - 1) * pageSize

        if (firstOfPageIndex > repo.size - 1) {
            return Observable.just(ArrayList<GithubReposResponse>() as List<GithubReposResponse>).delay(1, TimeUnit.SECONDS)
        }

        val lastIndex = Math.min(firstOfPageIndex + pageSize, repo.size)

        return Observable.just(repo.subList(firstOfPageIndex, lastIndex)).delay(1, TimeUnit.SECONDS)
    }

    override fun getUserInfo(userName: String): Observable<GithubUserResponse> {
        return Observable.just(GithubUserResponse("jakeWharton", 66577,
                "https://avatars0.githubusercontent.com/u/66577?v=4", "",
                "", "https://github.com/JakeWharton", "", "",
                "", "", "", "",
                "", "", "", "",
                false, "Jake Wharton", "", "http://jakewharton.com",
                "", "jakewharton@gmail.com", "", "Jake is one of the best android developers ever. " +
                "he has contributed to many big projects like dagger and butterknife.",
                1, 1, 1, 1,
                "", "")).delay(1, TimeUnit.SECONDS)
    }

}