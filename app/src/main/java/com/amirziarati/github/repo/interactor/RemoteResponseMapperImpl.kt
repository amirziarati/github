package com.amirziarati.github.repo.interactor

import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.repo.local.User
import com.amirziarati.githuhrepos.repo.remote.GithubReposResponse
import com.amirziarati.githuhrepos.repo.remote.GithubUserResponse
import javax.inject.Inject

class RemoteResponseMapperImpl @Inject constructor() : RemoteResponseMapper {
    override fun mapRepoResponse(repos: List<GithubReposResponse>): List<Repo> {
        val mappedRepos = ArrayList<Repo>()
        repos.forEach { item ->
            mappedRepos.add(Repo(item.id, item.name, item.description, item.stargazers_count, item.updated_at, item.license?.name))
        }
        return mappedRepos
    }

    override fun mapUserInfoResponse(userInfoResponse: GithubUserResponse): User {
        return User(userInfoResponse.id,
                userInfoResponse.login,
                userInfoResponse.name,
                userInfoResponse.avatar_url,
                userInfoResponse.email,
                if (!userInfoResponse.blog.isNullOrEmpty()) userInfoResponse.blog else userInfoResponse.html_url,
                userInfoResponse.bio)

    }
}