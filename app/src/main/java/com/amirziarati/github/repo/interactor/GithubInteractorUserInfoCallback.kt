package com.amirziarati.github.repo.interactor

import com.amirziarati.github.repo.local.User

interface GithubInteractorUserInfoCallback {
    fun onUserInfoReceived(user: User)
    fun onUserInfoFailed()
}
