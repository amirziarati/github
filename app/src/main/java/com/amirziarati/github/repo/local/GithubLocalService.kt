package com.amirziarati.github.repo.local

import io.reactivex.Observable

interface GithubLocalService {
    fun saveRepo(repo: Repo)
    fun saveRepos(repos: List<Repo>)
    fun getRepos(userName: String): List<Repo>
    fun getReposList(userName: String, page: Int, pageSize: Int): Observable<List<Repo>>
    fun saveUser(user: User)
    fun getUser(username: String): Observable<User>
}