package com.amirziarati.github.repo.interactor

import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.repo.local.User
import com.amirziarati.githuhrepos.repo.remote.GithubReposResponse
import com.amirziarati.githuhrepos.repo.remote.GithubUserResponse

interface RemoteResponseMapper {
    fun mapRepoResponse(repos: List<GithubReposResponse>): List<Repo>
    fun mapUserInfoResponse(userInfoResponse: GithubUserResponse): User
}