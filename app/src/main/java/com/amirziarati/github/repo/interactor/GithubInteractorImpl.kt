package com.amirziarati.github.repo.interactor

import com.amirziarati.github.repo.local.GithubLocalService
import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.repo.local.User
import com.amirziarati.github.utils.ConnectionManager
import com.amirziarati.github.utils.SchedulersProvider
import com.amirziarati.githuhrepos.repo.remote.GithubRemoteService
import com.amirziarati.githuhrepos.repo.remote.GithubReposResponse
import com.amirziarati.githuhrepos.repo.remote.GithubUserResponse
import io.reactivex.Observable
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


class GithubInteractorImpl @Inject constructor(private val githubRemoteService: GithubRemoteService,
                                               private val githubLocalService: GithubLocalService,
                                               private val connectionManager: ConnectionManager,
                                               private val schedulersProvider: SchedulersProvider,
                                               private val remoteResponseMapper: RemoteResponseMapper) : GithubInteractor {

    override fun getRepos(userName: String, page: Int, pageSize: Int, repoCallback: GithubInteractorRepoCallback) {

        if (connectionManager.isConnected()) {
            githubRemoteService.getReposList(userName, page, pageSize)
                    .subscribeOn(schedulersProvider.ioThread())
                    .flatMap { repos ->
                        val mappedRepos = remoteResponseMapper.mapRepoResponse(repos)
                        githubLocalService.saveRepos(mappedRepos)
                        Observable.just(mappedRepos)
                    }
                    .observeOn(schedulersProvider.mainThread())
                    .subscribe(
                            { repos ->
                                repoCallback.onReposReceived(repos)
                            },
                            { error ->
                                handleError(error, repoCallback)
                                getCachedRepos(userName, page, pageSize, repoCallback)
                            })

        } else {
            repoCallback.onInternetUnavailable()
            getCachedRepos(userName, page, pageSize, repoCallback)
        }

    }

    override fun getUserInfo(userName: String, userInfoCallback: GithubInteractorUserInfoCallback) {
        if (connectionManager.isConnected()) {
            githubRemoteService.getUserInfo(userName)
                    .subscribeOn(schedulersProvider.ioThread())
                    .flatMap {
                        val user = remoteResponseMapper.mapUserInfoResponse(it)
                        githubLocalService.saveUser(user)
                        Observable.just(user)
                    }
                    .observeOn(schedulersProvider.mainThread())
                    .subscribe({ user -> userInfoCallback.onUserInfoReceived(user) },
                            { error ->
                                getCachedUserInfo(userName, userInfoCallback)
                            })
        } else {
            getCachedUserInfo(userName, userInfoCallback)
        }
    }


    private fun handleError(error: Throwable?, repoCallback: GithubInteractorRepoCallback) {
        if (error is HttpException) {
            when (error.code()) {
                403 -> repoCallback.onExeededApiCallLimit()
                500 -> repoCallback.onInternalServerError()
            }
        } else if (error is IOException) {
            repoCallback.onServerUnreachable()
        }
    }

    private fun getCachedRepos(userName: String, page: Int, pageSize: Int, repoCallback: GithubInteractorRepoCallback) {
        githubLocalService.getReposList(userName, page, pageSize)
                .subscribeOn(schedulersProvider.ioThread())
                .observeOn(schedulersProvider.mainThread())
                .subscribe({ cachedRepos ->
                    repoCallback.onReposReceived(cachedRepos)
                },
                        { error ->
                            repoCallback.onReposReceived(emptyList())
                        })
    }

    private fun getCachedUserInfo(userName: String, userInfoCallback: GithubInteractorUserInfoCallback) {
        githubLocalService.getUser(userName)
                .subscribeOn(schedulersProvider.ioThread())
                .observeOn(schedulersProvider.mainThread())
                .subscribe({ cachedUser ->
                    userInfoCallback.onUserInfoReceived(cachedUser)
                },
                        { error ->
                            userInfoCallback.onUserInfoFailed()
                        })
    }



}