package com.amirziarati.github.ui.repolist

import com.amirziarati.github.repo.interactor.GithubInteractor
import com.amirziarati.github.repo.interactor.GithubInteractorRepoCallback
import com.amirziarati.github.repo.interactor.GithubInteractorUserInfoCallback
import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.repo.local.User
import com.amirziarati.github.utils.NavigationManager
import javax.inject.Inject

class RepoPresenterImpl @Inject constructor(private val view: RepoView,
                                            private val githubInteractor: GithubInteractor,
                                            private val navigationManager: NavigationManager) : RepoPresenter, GithubInteractorRepoCallback {

    private var page: Int = 1
    private var pageSize: Int = 15
    private var userName: String = ""
    private var user: User? = null
    private var isLoading = false
    private var isAllReposLoaded = false

    override fun onInit(userName: String,
                        pageSize: Int) {

        this.userName = userName
        this.pageSize = pageSize
        this.page = 1
        this.isLoading = false
        this.isAllReposLoaded = false

        getUserInfo(userName)
        onNewPageOfReposRequested()
    }



    override fun onNewPageOfReposRequested() {
        if (userName == "")
            throw Exception("onInit was not called")

        if (isLoading || isAllReposLoaded)
            return

        isLoading = true

        if (page == 1)
            view.showProgress()

        view.showLoadMoreProgress()

        githubInteractor.getRepos(userName, page, pageSize, this)

    }

    override fun onRefreshRepoDataRequested() {
        view.clearReposList()
        onInit(userName, pageSize)
    }

    override fun onReposReceived(repos: List<Repo>) {
        view.hideLoadMoreProgress()
        view.hideProgress()
        if (page == 1 && repos.isEmpty())
            view.showEmptyListMessage()
        else {
            view.hideEmptyListMessage()
            view.showRepos(repos)
            page++
            if (repos.size < pageSize)
                isAllReposLoaded = true
        }
        isLoading = false
    }

    override fun onMailToUserRequested() {
        if (user != null && !user?.mail.isNullOrEmpty()) {
            navigationManager.showMailToView(user?.mail!!)
        } else {
            view.showUserDataNotAvailableError()
        }
    }

    override fun onGoToUserBlogRequested() {
        if (user != null && !user?.blog.isNullOrEmpty()) {
            navigationManager.showWebPage(user?.blog!!)
        } else {
            view.showUserDataNotAvailableError()
        }
    }

    override fun onInternetUnavailable() {
        view.hideProgress()
        view.hideLoadMoreProgress()
        view.showConnectionError()
        isLoading = false
    }

    override fun onServerUnreachable() {
        view.hideProgress()
        view.hideLoadMoreProgress()
        view.showServerUnreachableError()
        isLoading = false
    }

    override fun onExeededApiCallLimit() {
        view.hideProgress()
        view.hideLoadMoreProgress()
        view.showServerError()
        isLoading = false
    }

    override fun onInternalServerError() {
        view.hideProgress()
        view.hideLoadMoreProgress()
        view.showServerError()
        isLoading = false
    }

    private fun getUserInfo(userName: String) {
        githubInteractor.getUserInfo(userName, object : GithubInteractorUserInfoCallback {

            override fun onUserInfoReceived(user: User) {
                this@RepoPresenterImpl.user = user

                val displayName = user.displayName
                if (!displayName.isNullOrEmpty())
                    view.showUserDisplayName(displayName!!)
                else
                    view.showUserDisplayName(userName)

                val imageUrl = user.avatar
                if (!imageUrl.isNullOrEmpty())
                    view.showUserAvatar(imageUrl!!)

                val bio = user.bio
                if (!bio.isNullOrEmpty())
                    view.showUserBio(bio!!)
                else
                    view.showNoBio()
            }

            override fun onUserInfoFailed() {
                view.showUserDisplayName(userName)
            }

        })
    }

}