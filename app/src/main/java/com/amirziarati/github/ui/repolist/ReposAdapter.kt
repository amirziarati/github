package com.amirziarati.github.ui.repolist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amirziarati.github.R
import com.amirziarati.github.repo.local.Repo
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_repo.*


class ReposAdapter(private val context: Context, private val repos: List<Repo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private var isLoading = false
    private val VIEWTYPE_ITEM = 1
    private val VIEWTYPE_PROGRESS = 2


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEWTYPE_ITEM)
            RepoHolder(LayoutInflater.from(context).inflate(R.layout.item_repo, parent, false))
        else
            ProgressHolder(LayoutInflater.from(context).inflate(R.layout.item_progress, parent, false))
    }

    override fun getItemCount(): Int {
        return repos.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoading && position >= repos.size - 1)
            VIEWTYPE_PROGRESS
        else
            VIEWTYPE_ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder is RepoHolder) {
            holder.let {
                holder.tvRepoName?.text = repos[position].name
                holder.tvRepoDescription?.text = repos[position].description
                holder.tvRepoStars?.text = repos[position].starsCount.toString()
                holder.tvRepoUpdated?.text = repos[position].updatedAt.toString()
                val license = repos[position].license
                holder.tvRepoLicense?.text =  if (!license.isNullOrBlank()) license else context.getString(R.string.no_license_text)
            }
        }
    }

    fun addRepos(newRepos: List<Repo>) {
        val previousReposSize = this.repos.size
        (this.repos as ArrayList<Repo>).addAll(newRepos)
        notifyItemRangeInserted(previousReposSize, newRepos.size)
    }


    var fakeObjectIndex = -1
    fun showProgress() {
        if (fakeObjectIndex != -1)
            return
        isLoading = true
        fakeObjectIndex = repos.size
        (this.repos as ArrayList<Repo>).add(Repo())
        notifyItemInserted(fakeObjectIndex)
    }


    fun hideProgress() {
        if (fakeObjectIndex == -1)
            return
        isLoading = false
        (this.repos as ArrayList<Repo>).removeAt(fakeObjectIndex)
        notifyItemRemoved(fakeObjectIndex)
        fakeObjectIndex = -1
    }

    fun clearRepos() {
        val previousReposSize = repos.size
        (repos as ArrayList<Repo>).clear()
        notifyItemRangeRemoved(0, previousReposSize)
    }

    class RepoHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer

    class ProgressHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer
}