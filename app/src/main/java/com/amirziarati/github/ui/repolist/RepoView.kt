package com.amirziarati.github.ui.repolist

import com.amirziarati.github.repo.local.Repo

interface RepoView {
    fun showProgress()
    fun showConnectionError()
    fun showServerUnreachableError()
    fun showServerError()
    fun hideProgress()
    fun showLoadMoreProgress()
    fun hideLoadMoreProgress()
    fun showRepos(repos: List<Repo>)
    fun showEmptyListMessage()
    fun hideEmptyListMessage()
    fun clearReposList()
    fun showUserDisplayName(displayName: String)
    fun showUserDataNotAvailableError()
    fun showUserAvatar(imageUrl: String)
    fun showUserBio(bio: String)
    fun showNoBio()
}