package com.amirziarati.github.ui.repolist

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.amirziarati.github.R
import com.amirziarati.github.di.DaggerMyApplicationComponent
import com.amirziarati.github.di.MyApplicationModule
import com.amirziarati.github.di.RepoModule
import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.utils.Configs
import com.amirziarati.github.utils.EndlessScrollListener
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class ReposListActivity : AppCompatActivity(), RepoView {

    @Inject
    lateinit var presenter: RepoPresenter

    private var reposAdapter: ReposAdapter? = null
    private fun injectDependencies() {
        DaggerMyApplicationComponent
                .builder()
                .repoModule(RepoModule(this))
                .myApplicationModule(MyApplicationModule(application))
                .build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initView()

        presenter.onInit(Configs.DEFAULT_USER_NAME, Configs.PAGE_SIZE)
    }

    private fun initView() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)


        val linearLayoutManager = LinearLayoutManager(this@ReposListActivity)
        rvRepos.layoutManager = linearLayoutManager



        tvRetry.setOnClickListener { presenter.onRefreshRepoDataRequested() }

        fab.setOnClickListener {
            presenter.onMailToUserRequested()
        }

        swipeRefresh.setOnRefreshListener {
            presenter.onRefreshRepoDataRequested()
        }

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark)
        appBarLayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            swipeRefresh.isEnabled = (verticalOffset == 0)

            changeAppbarViewsLocation(verticalOffset)
        }

    }

    private var lastViewUpdate: Long = 0
    private fun changeAppbarViewsLocation(verticalOffset: Int) {
        appBarLayout.post {
            if (System.currentTimeMillis() - lastViewUpdate > 10) {
                lastViewUpdate = System.currentTimeMillis()
                val appBarHeight = resources.getDimension(R.dimen.app_bar_height)
                val verticalOffsetAbs = Math.abs(verticalOffset)
                if (verticalOffsetAbs < appBarHeight) {
                    val q: Float = appBarHeight * 1.33f / (appBarHeight * 1.33f - verticalOffsetAbs)

                    imgUser.scaleX = 1 / q
                    imgUser.scaleY = 1 / q

                    val leftMargin = resources.getDimension(R.dimen.display_name_left_margin)
                    val imgBottomMargin = resources.getDimension(R.dimen.margin_bottom_img_user)

                    val tvNameNewMargin: Int = (leftMargin + (100 * (q - 1))).toInt()
                    val imgUserNewLeftMargin: Int = (leftMargin / (q * 3)).toInt()
                    val imgUserNewBottomMargin: Int = Math.min(imgBottomMargin / (q * 0.58f), imgBottomMargin).toInt()

                    val params = tvUserDisplayName.layoutParams as CollapsingToolbarLayout.LayoutParams
                    params.setMargins(tvNameNewMargin, params.topMargin, params.rightMargin, params.bottomMargin) //substitute parameters for left, top, right, bottom
                    tvUserDisplayName.layoutParams = params

                    val params2 = imgUser.layoutParams as CollapsingToolbarLayout.LayoutParams
                    params2.setMargins(imgUserNewLeftMargin, params2.topMargin, params2.rightMargin, imgUserNewBottomMargin) //substitute parameters for left, top, right, bottom
                    imgUser.layoutParams = params2
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_exit -> {
                finish()
                true
            }
            R.id.action_go_to_web_page -> {
                presenter.onGoToUserBlogRequested()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showRepos(repos: List<Repo>) {
        if (reposAdapter == null) {
            reposAdapter = ReposAdapter(this@ReposListActivity, repos)
            rvRepos.adapter = reposAdapter
            rvRepos.addOnScrollListener(object : EndlessScrollListener(rvRepos.layoutManager as LinearLayoutManager, Configs.LIST_TRESHHOLD_COUNT) {
                override fun onLoadMore(current_page: Int) {
                    presenter.onNewPageOfReposRequested()
                }
            })
        } else {
            rvRepos.post {
                reposAdapter?.addRepos(repos)
            }
        }
    }

    override fun clearReposList() {
        rvRepos.post {
            reposAdapter?.clearRepos()
            reposAdapter = null
        }
    }

    override fun showProgress() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefresh.isRefreshing = false
    }

    override fun showLoadMoreProgress() {
        rvRepos.post {
            reposAdapter?.showProgress()
        }
    }

    override fun hideLoadMoreProgress() {
        rvRepos.post {
            reposAdapter?.hideProgress()
        }
    }

    override fun showConnectionError() {
        showSnack(getString(R.string.connection_message))
    }

    override fun showServerUnreachableError() {
        showSnack(getString(R.string.server_unreachable_message))
    }

    override fun showServerError() {
        showSnack(getString(R.string.server_error_message))
    }

    override fun showUserDataNotAvailableError() {
        showSnack(getString(R.string.no_user_data_available_message))
    }

    override fun showEmptyListMessage() {
        tvEmptyList.visibility = View.VISIBLE
        tvRetry.visibility = View.VISIBLE
    }

    override fun hideEmptyListMessage() {
        tvEmptyList.visibility = View.GONE
        tvRetry.visibility = View.GONE
    }

    override fun showUserDisplayName(displayName: String) {
        tvUserDisplayName.text = displayName
    }

    override fun showUserBio(bio: String) {
        tvBio.text = bio
    }

    override fun showNoBio() {
        tvBio.text = getString(R.string.no_bio_message)
    }

    override fun showUserAvatar(imageUrl: String) {
        val requestOptions: RequestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.avatar)
        requestOptions.fallback(R.drawable.avatar)
        requestOptions.error(R.drawable.avatar)
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(imageUrl).apply(RequestOptions.circleCropTransform()).into(imgUser)
    }

    private fun showSnack(message: String) {
        Snackbar.make(rvRepos, message, Snackbar.LENGTH_LONG).show()
    }
}
