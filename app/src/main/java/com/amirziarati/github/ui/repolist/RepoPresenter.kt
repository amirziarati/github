package com.amirziarati.github.ui.repolist

interface RepoPresenter {
    fun onInit(userName : String,
               pageSize : Int)

    fun onNewPageOfReposRequested()
    fun onRefreshRepoDataRequested()
    fun onMailToUserRequested()
    fun onGoToUserBlogRequested()
}