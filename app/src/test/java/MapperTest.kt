import com.amirziarati.github.repo.interactor.*

import com.amirziarati.githuhrepos.repo.remote.GithubUserResponse
import org.junit.Test

import org.junit.Assert.*



/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MapperTest {

    private val mapper = RemoteResponseMapperImpl()

    @Test
    fun testUserMapper() {
        val userinfoRespons = GithubUserResponse("jakeWharton", 66577,
                "https://avatars0.githubusercontent.com/u/66577?v=4", "",
                "", "https://github.com/JakeWharton", "", "",
                "", "", "", "",
                "", "", "", "",
                false, "Jake Wharton", "", "http://jakewharton.com",
                "", "jakewharton@gmail.com", "", "Jake is one of the best android developers ever. " +
                "he has contributed to many big projects like dagger and butterknife.",
                1, 1, 1, 1,
                "", "")

        val user = mapper.mapUserInfoResponse(userinfoRespons)
        assertEquals(userinfoRespons.name, user.displayName)
        assertEquals(userinfoRespons.email, user.mail)
        assertEquals(userinfoRespons.login, user.userName)
        assertEquals(userinfoRespons.avatar_url, user.avatar)
        assertEquals(userinfoRespons.id, user.id)
        assertEquals(userinfoRespons.bio, user.bio)
        assertEquals(userinfoRespons.blog, user.blog)

    }

    @Test
    fun testEmptyBlogUserMap() {
        val emptyBioEmptyBlog = GithubUserResponse("jakeWharton", 66577,
                "https://avatars0.githubusercontent.com/u/66577?v=4", "",
                "", "https://github.com/JakeWharton", "", "",
                "", "", "", "",
                "", "", "", "",
                false, "Jake Wharton", "", "",
                "", "jakewharton@gmail.com", "", "",
                1, 1, 1, 1,
                "", "")

        val user = mapper.mapUserInfoResponse(emptyBioEmptyBlog)
        assertEquals(emptyBioEmptyBlog.html_url, user.blog)
    }





}
