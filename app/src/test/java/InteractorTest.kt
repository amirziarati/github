import com.amirziarati.github.repo.interactor.GithubInteractor
import com.amirziarati.github.repo.interactor.GithubInteractorRepoCallback
import com.amirziarati.github.repo.local.Repo
import com.amirziarati.github.ui.repolist.RepoPresenter
import com.amirziarati.github.ui.repolist.RepoPresenterImpl
import com.amirziarati.github.ui.repolist.RepoView
import com.amirziarati.github.utils.ConnectionManager
import com.amirziarati.github.utils.NavigationManager

import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import org.mockito.stubbing.Answer

class InteractorTest {

    private var repoPresenter: RepoPresenterImpl? = null

    @Mock
    private var githubInteractor: GithubInteractor? = null

    @Mock
    private val mockView: RepoView? = null

    @Mock
    private val mockNavigationManager: NavigationManager? = null


    @Mock
    private val mockConnectionManager: ConnectionManager? = null


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        repoPresenter = RepoPresenterImpl(mockView!!, githubInteractor!!, mockNavigationManager!!)
    }

    //resolve null problem of kotlin with matchers
    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)
    private fun <T> eq(obj: T): T = Mockito.eq<T>(obj)
    private fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()
    private inline fun <reified T : Any> argumentCaptor(): ArgumentCaptor<T> =
            ArgumentCaptor.forClass(T::class.java)


    @Test
    fun testIfOnInternetUnavailableCallbackIsCalled() {
        Mockito.`when`(mockConnectionManager!!.isConnected()).thenReturn(false)

        Mockito.doAnswer(Answer<Any?> { invocation ->
            if (!mockConnectionManager!!.isConnected())
                (invocation!!.arguments[3] as GithubInteractorRepoCallback).onInternetUnavailable()
            null
        })!!.`when`(githubInteractor)!!
                .getRepos(any(String::class.java),
                        any(Int::class.java),
                        any(Int::class.java), any(GithubInteractorRepoCallback::class.java))


        val repoSpy = spy(repoPresenter)

        repoSpy!!.onInit("some one", 0)


        verify(repoSpy!!, times(1)).onInternetUnavailable()

        verify(mockView!!, times(1)).showConnectionError()

    }
}